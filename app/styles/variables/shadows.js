import getColor from './getColor';

const shadows = {
  message: `0 4px 6px 0 ${getColor('black', 0.05)}`,
};

export default shadows;
