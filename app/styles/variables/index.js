import getColorTmp from './getColor';
import shadowsTmp from './shadows';

export const getColor = getColorTmp;
export const shadows = shadowsTmp;

export default {
  getColor,
  shadows,
};
