const colorFabric = (colorName, opacity = 1) => {
  /* eslint-disable no-multi-spaces */
  const colors = {
    white: '255, 255, 255',       // #FFFFFF
    black: '0, 0, 0',             // 000000
    turquoise: '26, 188, 156',    // #1abc9c
    green_sea: '22, 160, 133',    // #16a085
    sunflower: '241, 196, 15',    // #f1c40f
    orange: '243, 156, 18',       // #f39c12
    emerald: '46, 204, 113',      // #2ecc71
    nephritis: '39, 174, 96',     // #27ae60
    carrot: '230, 126, 34',       // #e67e22
    pumpkin: '211, 84, 0',        // #d35400
    petter_river: '52, 152, 219', // #3498db
    belize_hole: '41, 128, 185',  // #2980b9
    alizarian: '231, 76, 60',     // #e74c3c
    pomegranate: '192, 57, 43',   // #c0392b
    amethyst: '155, 89, 182',     // #9b59b6
    wisteria: '142, 68, 173',     // #8e44ad
    clouds: '236, 240, 241',      // #ecf0f1
    silver: '189, 195, 199',      // #bdc3c7
    wet_asphalt: '52, 73, 94',    // #34495e
    midnight_blue: '44, 62, 80',  // #2c3e50
    concrete: '149, 165, 166',    // #95a5a6
    sbestos: '127, 140, 141',     // #7f8c8d
    /* eslint-enable no-multi-spaces */
  };

  const color = colors[colorName];

  return `rgba(${color}, ${opacity})`;
};

export default colorFabric;
