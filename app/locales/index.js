import LocalizedStrings from 'react-localization';
import ru from './ru.json';
import en from './en.json';

const locales = new LocalizedStrings({ ru, en });

export default locales;
