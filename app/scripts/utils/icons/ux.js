import styled from 'styled-components';

import Close from 'img_icons/close.svg';

export const CloseIcon = styled(Close)`
  pointer-events: none;
`;
