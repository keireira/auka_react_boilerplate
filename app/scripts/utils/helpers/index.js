import actionsTMP from './actions';
import canSetLangTMP from './canSetLang';
import getCurrentLocaleTMP from './getCurrentLocale';
import reduceHandlersTMP from './reducerHandlers';

export const actions = actionsTMP;
export const canSetLang = canSetLangTMP;
export const reduceHandlers = reduceHandlersTMP;
export const getCurrentLocale = getCurrentLocaleTMP;

export default {
  actions,
  canSetLang,
  reduceHandlers,
  getCurrentLocale,
};
