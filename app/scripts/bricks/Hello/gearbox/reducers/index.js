import { createReducer } from 'redux-act';
import deposits from './deposits';

const initialState = {
  isFetching: false,
  error: null,
  accounts: [],
  deposits: [],
  account: {
    history: [],
  },
  deposit: {
    history: [],
  },
};

const reducer = createReducer({
  ...deposits,
}, initialState);

export default reducer;
