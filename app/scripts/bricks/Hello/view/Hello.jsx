import React from 'react';
import locales from 'locales';

import StyledHello from './styles';

class Hello extends React.PureComponent {
  render() {
    return (
      <StyledHello>
        {locales.hello}
      </StyledHello>
    );
  };
};

export default Hello;
