import styled from 'styled-components';
import { toRem } from 'styles';

export default styled.div`
  font-weight: bold;
  font-size: ${toRem(64)};

  display: flex;
  width: 100vw;
  height: 100vh;
  align-items: center;
  justify-content: center;
`;
