import locale from './locale';

export const localesAct = locale;

export default {
  ...locale,
};
