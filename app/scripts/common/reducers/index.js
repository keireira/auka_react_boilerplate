import locale from 'reducers/locale';

const rootReducer = {
  locale,
};

export default rootReducer;
