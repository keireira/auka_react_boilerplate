import React from 'react';
import { routeNode } from 'react-router5';

import IndexTemplate from './styles';
import Hello from 'bricks/Hello';

const RootTemplate = () => {
  return (
    <IndexTemplate>
      <Hello />
    </IndexTemplate>
  );
};

export default routeNode('root')(RootTemplate);
