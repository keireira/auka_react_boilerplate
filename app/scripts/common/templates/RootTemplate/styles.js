import styled from 'styled-components';
import { sizes } from 'styles/utils/media';

export default styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: flex-end;

  width: 100%;
  height: 100%;
  max-width: ${sizes.alpha}px;
  background-color: ${({ theme }) => theme.getColor('wet_asphalt')};
`;
