import React from 'react';
import { constants } from 'router5';
import { routeNode } from 'react-router5';

import StyledNotFound from './styles';

class NotFound extends React.PureComponent {
  render() {
    return (
      <StyledNotFound>
        404
      </StyledNotFound>
    );
  };
};

export default routeNode(constants.UNKNOWN_ROUTE)(NotFound);
