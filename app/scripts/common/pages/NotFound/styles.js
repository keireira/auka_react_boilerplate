import styled from 'styled-components';
import { toRem } from 'styles';

export default styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;

  font-size: ${toRem(90)};
`
