import localeTMP from './locale';

export const locale = localeTMP;

export default {
  ...locale,
};
