import createRouter from 'router5';
import loggerPlugin from 'router5/plugins/logger';
import browserPlugin from 'router5/plugins/browser';
import listenersPlugin from 'router5/plugins/listeners';
import routes from './routes';

const routerCreator = () => {
  const router = createRouter(routes, {
    allowNotFound: true,
  });

  // Plugins
  router.usePlugin(loggerPlugin);

  router.usePlugin(browserPlugin({
    useHash: false,
  }));

  router.usePlugin(listenersPlugin());

  return router;
};

export default routerCreator;
