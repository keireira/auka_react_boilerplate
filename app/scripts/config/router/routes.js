import { constants } from 'router5';

const routes = [
  {
    name: constants.UNKNOWN_ROUTE,
    params: { path: '*' },
    path: '/notfound',
  },
  {
    name: 'root',
    path: '/',
  },
];

export default routes;
