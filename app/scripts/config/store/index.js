import { createStore, combineReducers } from 'redux';
import { router5Reducer } from 'redux-router5';

import rootReducer from 'reducers';
import { runSagas, createEnhancers } from './enhancers';

export default function configureStore(router, initialState) {
  const enhancers = createEnhancers(router);

  const store = createStore(
    combineReducers({
      ...rootReducer,
      router: router5Reducer,
    }),
    initialState,
    enhancers
  );

  runSagas();

  if (module.hot && process.env.NODE_ENV === 'development') {
    module.hot.accept('reducers', () => {
      const nextRootReducer = require('reducers').default;

      store.replaceReducer(combineReducers({
        ...nextRootReducer,
        router: router5Reducer,
      }));
    });
  }

  return store;
};
