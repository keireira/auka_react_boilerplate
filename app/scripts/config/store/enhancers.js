import createSagaMiddleware from 'redux-saga';
import { applyMiddleware, compose } from 'redux';
import { router5Middleware } from 'redux-router5';

import rootSaga from 'sagas';
import devModeCompose from 'config/store/dev';
import composeMiddlewares from 'config/store/composeMiddlewares';

const sagaMiddleware = createSagaMiddleware();

const isDev = process.env.NODE_ENV === 'development';

export const createEnhancers = (router) => {
  const routerMiddleware = router5Middleware(router);

  const middlewares = composeMiddlewares([
    routerMiddleware,
    sagaMiddleware,
  ], isDev);

  const composeEnhancers = (isDev) ? devModeCompose : compose;

  const enhancers = composeEnhancers(
    applyMiddleware(...middlewares)
  );

  return enhancers;
};

export const runSagas = () => {
  sagaMiddleware.run(rootSaga);

  return sagaMiddleware;
};
