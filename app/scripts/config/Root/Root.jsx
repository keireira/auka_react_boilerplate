import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { Provider } from 'react-redux';
import { RouterProvider } from 'react-router5';
import { ThemeProvider } from 'styled-components';

import locales from 'locales';
import { Root } from './styles';
import { getColor } from 'styles/variables';
import 'styles';

import RootTemplate from 'templates/RootTemplate';

const theme = {
  getColor,
};

const RootComponent = ({ store, router }) => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <RouterProvider router={router}>
          <Root>
            <Helmet>
              <title>{locales.title}</title>
              <meta name="viewport" content="width=device-width, initial-scale=1" />
              <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
            </Helmet>

            <RootTemplate />
          </Root>
        </RouterProvider>
      </ThemeProvider>
    </Provider>
  );
};

RootComponent.propTypes = {
  store: PropTypes.object.isRequired,
  router: PropTypes.object,
};

RootComponent.defaultProps = {
  store: {},
  router: {},
};

export default RootComponent;
