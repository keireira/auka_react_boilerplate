import styled from 'styled-components';
import { toRem } from 'styles';

export const Root = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;

  font-size: ${toRem(16)};
  line-height: ${toRem(18)};
  font-family: 'roboto-regular';
  color: ${({ theme }) => theme.getColor('clouds')};

  background-color: ${({ theme }) => theme.getColor('clouds')};
`;
