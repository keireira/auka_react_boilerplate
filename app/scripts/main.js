import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import locales from 'locales';
import Root from 'config/Root';
import createRouter from 'config/router';
import configureStore from 'config/store';

const initialState = {
  locale: {
    currentLocale: 'ru',
  },
};

const router = createRouter();
const store = configureStore(router, initialState);
locales.setLanguage(initialState.locale.currentLocale);

DOMRender();

if (module.hot) {
  module.hot.accept('config/Root', () => {
    DOMRender();
  });
}

function DOMRender() {
  ReactDOM.render(
    <AppContainer>
      <Root store={store} router={router} />
    </AppContainer>,
    document.querySelector('#app')
  );
};
