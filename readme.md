# Auka React boilerplate (Windows is not supported!)
  - React
  - Redux
  - Router5
  - Sagas
  - Styled components
  - HMR
  - Webpack 3
  - Jest
  - ESLint (airbnb)
  - Stylelint
  - Localization (react-localize)

## Start project
```shell
git@gitlab.com:alena.wop.wops/auka_react_boilerplate.git project
cd ./project
yarn run init
```

## Running into dev mode
```shell
yarn run dev
```

## Running into build mode
```shell
yarn run build
```

## Run tests
```shell
yarn run test
yarn run test:report # с выводом отчёта
```

## Clear trash files
```shell
yarn run clean
```

### TODO
- [ ] SSR
- [ ] WS mock
- [ ] manifest
- [ ] moduleResolver
- [ ] resolve.modules
- [ ] source maps
- [ ] Ports checking
- [ ] Packet manager detection
