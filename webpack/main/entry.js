const aliases = require('../aliases');
const consts = require('../constants');
const NODE_ENV = process.env.NODE_ENV.toLowerCase();

const entry = {
  vendor: [],
  main: [
    'babel-polyfill',
  ],
};

if (NODE_ENV === 'development') {
  entry.main.push(
    'react-hot-loader/patch',
    `webpack-dev-server/client?http://${consts.HOST}:${consts.PORT}`,
    'react-dev-utils/webpackHotDevClient',
    'webpack/hot/dev-server'
  );
}

entry.main.push(
  aliases.entryPoint
);

entry.vendor.push(
  'router5',
  'react-router5',
  'redux-router5',

  'react',
  'react-dom',
  'prop-types',
  'react-helmet',
  'react-localization',

  'redux',
  'redux-act',
  'redux-saga',
  'react-redux',

  'reselect',
  'styled-components',
);

module.exports = entry;
