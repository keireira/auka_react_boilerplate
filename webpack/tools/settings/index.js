const path = require('path');
const spawnConfig = require('./spawnConfig.json');

process.env.PM = 'npm';
process.env.defaultName = path.basename(path.resolve(__dirname, '../../../'));

module.exports = {
  spawnConfig,
};
