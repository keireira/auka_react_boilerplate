const fs = require('fs');
const path = require('path');
const { spawn, exec } = require('child_process');

const { getFields, getFontColor } = require('../helpers');
const { spawnConfig } = require('../settings');

// Import future package.json config
const jest = require('./configs/jest.json');
const scripts = require('./configs/scripts.json');
const engines = require('./configs/engines.json');
const dependencies = require('./configs/dependencies.json');
const devDependencies = require('./configs/devDependencies.json');

const generatePackageJson = () => {
  const prompt = require('prompt');
  const colors = require('colors/safe');
  const jsonBeautify = require('json-beautify');

  // Prompt setup
  prompt.message = colors[getFontColor()].bgBlack.bold('(*^‿^*)');
  prompt.delimiter = colors.green.bgBlack.bold(': ');
  prompt.start();

  prompt.get(getFields(), (err, result) => {
    const newPackageJson = {
      name: result.project_name || process.env.defaultName,
      version: result.version || '0.0.1',
      description: result.description || '',
      scripts, engines, jest,
      author: {
        name: result.author_name || '',
        email: result.email || '',
        url: result.url || '',
      },
      main: result.main || 'webpack.config.js',
      license: result.license || 'MIT',
      dependencies, devDependencies,
    };

    const beautifiedPackageJson = jsonBeautify(newPackageJson, null, 2, 80);
    fs.writeFile(path.join(__dirname, '../../../package.json'), beautifiedPackageJson);

    // Remove tools for scaffolding
    exec('rm -rf ./webpack/tools/generatePackageJson ./webpack/tools/settings ./webpack/tools/index.js ', () => {
      spawn(process.env.PM, ['install'], spawnConfig);
    });
  });
};

module.exports = generatePackageJson;
