const getFields = () => {
  const colors = require('colors/safe');
  
  const textWrapper = (text) => colors.white.bgBlack.reset(text);

  return [
    {
      name: 'project_name',
      description: textWrapper(`Project name (${process.env.defaultName})`),
      pattern: /^[^._][a-z0-9\.\-_~]+$/,
    },
    {
      name: 'version',
      description: textWrapper('Version (0.0.1)'),
      pattern: /\d\.\d\.\d/g,
    },
    {
      name: 'description',
      description: textWrapper('Description'),
      pattern: /.{0,128}/g,
    },
    {
      name: 'main',
      description: textWrapper(`Entry point (webpack.config.js)`),
      pattern: /.{0,64}/g,
    },
    {
      name: 'license',
      description: textWrapper(`License (MIT)`),
      pattern: /.{0,32}/g,
    },
    {
      name: 'author_name',
      description: textWrapper('Author\'s name'),
      pattern: /.{0,128}/g,
    },
    {
      name: 'email',
      description: textWrapper('Author\'s E-mail'),
      pattern: /.{0,128}/g,
    },
    {
      name: 'url',
      description: textWrapper('Author\'s site'),
      pattern: /.{0,128}/g,
    },
  ];
};

module.exports = getFields;
