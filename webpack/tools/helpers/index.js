const getFields = require('./getFields');
const showLocalIP = require('./showLocalIP');
const getFontColor = require('./getFontColor');
const spawnErrorCb = require('./spawnErrorCb');
const showExternalIP = require('./showExternalIP');
const colorizeString = require('./colorizeString');

module.exports = {
  getFields,
  showLocalIP,
  spawnErrorCb,
  getFontColor,
  showExternalIP,
  colorizeString,
};
