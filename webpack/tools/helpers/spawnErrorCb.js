const spawnErrorCb = (error) => {
  const errorUTF8 = error.toString('utf8');

  console.log(errorUTF8);
};

module.exports = spawnErrorCb;
