const getFontColor = () => {
  const fontColors = ['red', 'green', 'yellow', 'blue', 'magenta', 'cyan', 'white'];

  return fontColors[Math.round(0 - 0.5 + Math.random() * (6 - 0 + 1))];
};

module.exports = getFontColor;
