const readline = require('readline');
const { spawn, exec } = require('child_process');
const { spawnConfig } = require('./settings');
const { spawnErrorCb } = require('./helpers');
const generatePackageJson = require('./generatePackageJson');

(() => {
  /*
   * 1. Определяем какой пакетный менеджер используется. По умолчанию npm
   *    Если найден yarn => предлагаем выбор между yarn и npm
   */
  exec('which yarn', (error, stdout, stderr) => {
    if (stdout.length > 0) {
      const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
      });

      rl.question('Choose packet manager [yarn|npm] (default: yarn): ', (answer) => {
        process.env.PM = (answer !== 'npm') ? 'yarn' : 'npm';
        console.log(`${process.env.PM} will be used.`);

        rl.close();
        main();
      });
    } else {
      main();
    }
  });
})();

function main() {
  // 2. Удаляем ненужные файлы
  const rmrf = spawn('rm', ['-rf', './node_modules', './.git', './yarn.lock'], spawnConfig);

  rmrf.on('close', () => {
    // 3. Инициализируем гит
    spawn('git', ['init'], spawnConfig);

    // 4. Устанавливаем зависимости для разворачивания проекта
    const install = spawn(process.env.PM, ['install'], spawnConfig);

    // 5. Генерируем package.json после установки зависимостей
    install.on('close', generatePackageJson);
  });

  rmrf.on('error', spawnErrorCb);
};
